Source: emacs-git-gutter
Section: lisp
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Nicholas D Steeves <nsteeves@gmail.com>
Build-Depends: debhelper-compat (= 12)
             , dh-elpa
             , git <!nocheck>
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/emacsen-team/emacs-git-gutter
Vcs-Git: https://salsa.debian.org/emacsen-team/emacs-git-gutter.git
Homepage: https://github.com/syohex/emacs-git-gutter
Testsuite: autopkgtest-pkg-elpa

Package: elpa-git-gutter
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs
        , emacs25
Description: Emacs port of the Sublime Text plugin GitGutter 
 Git-gutter is an Emacs port of the Sublime Text plugin GitGutter.
 This package writes symbols to the left Emacs window margin, by default
 displaying '+' for newly added lines, '-' for deleted lines, and '=' for
 modified lines.
  Features:
   * Support for Git, Mercurial, Subversion, and Bazaar.
   * Asynchronous per-line status updates, whenever a file is saved.
   * Support for Tramp, linum-mode, and visual-line-mode.
   * Does not require vc-mode.
   * Does not require graphical Emacs (works in the console and via SSH).
   * Configurable symbols and faces for added, deleted, or modified lines.
   * Optional vertical separator column (nice to have in CLI)
   * Basic statistics API for unstaged hunks, both per-buffer and globally.
